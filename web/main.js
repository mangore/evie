const hidden = 'hidden';
const md = 'menu-dropdown';
const mdd = 'menu-dropdown-down';
const clickable = 'clickable';
const noselect = 'no-select';

const web_context_ccp  = '/ccp/';
const web_context_ccp_img  = '/ccp/img/';
const web_context_ccp_data = '/ccp/data/';

/**
 * /ccp/img/icons/ - export of UprisingVxx.xx_Icons.zip from https://developers.eveonline.com/resource
 * /ccp/img/types/ - export of UprisingVxx.xx_Types.zip from https://developers.eveonline.com/resource
 * /ccp/data - preformated JSON data
 * /api - REST services
 */

var mainRoute = null;
var cSellOrders = null;
var cBuyOrders  = null;
var router = null;

var show = function (id) {
    var label = $('#' + id);
    id = id.replace('market-group-label-', '');
    var item = $('#market-group-container-' + id);
    if (item.hasClass(hidden)) {
        item.removeClass(hidden);
        label.removeClass(md);
        label.addClass(mdd);
        item.children().each(function () {
            var icon = $(this).children().first().children().first();
            if (!icon.attr('src')) {
                icon.attr('src', icon.attr('src-data'));
                icon.on('error', function () {
                    $(this).attr('src', web_context_ccp_img + 'icons/5_64_10.png');
                })
            }
        });
    } else {
        item.addClass(hidden);
        label.removeClass(mdd);
        label.addClass(md);
    }
}

function makeTableSortable(tableId) {
    const table = $('#' + tableId);
    const headers = table.find('thead tr th');
    headers.each((index, col) => {
        let c = $(col);
        c.addClass(clickable);
        c.addClass(noselect);
        c.on('click', () => {
            sortTableColumn(tableId, index);
        });
    });
}

function sortTableColumn(tableId, columnIndex) {
    const table = $('#' + tableId);
    let tOrder = 'asc';
    const cols = Array.from(table.find('thead th'));
    const col = $(cols[columnIndex]);
    let nextNumeric = col.hasClass('sort-on-next-column-as-numeric');
    const asc = col.hasClass('asc');
    const desc = col.hasClass('dsc');
    const cIndex = nextNumeric ? columnIndex + 1 : columnIndex;

    if (asc) {
        col.removeClass('asc');
        col.addClass('dsc');
        tOrder = 'desc';
    } else if (desc) {
        col.removeClass('dsc');
        col.addClass('asc');
    } else {
        for (const c of cols) {
            $(c).removeClass('asc');
            $(c).removeClass('dsc');
        }
        col.addClass('asc');
    }
    
    const rows = Array.from(table.find('tbody tr'));
    const sortedRows = rows.sort((a, b) => {
        const aValue = a.cells[cIndex].textContent;
        const bValue = b.cells[cIndex].textContent;

        if (nextNumeric) {
            if (tOrder === 'asc') {
                return aValue - bValue;
            } else {
                return bValue - aValue;
            }
        } else {
            if (tOrder === 'asc') {
                return aValue.localeCompare(bValue);
            } else {
                return bValue.localeCompare(aValue);
            }
        }
    });
    const t = document.getElementById(tableId);
    t.tBodies[0].append(...sortedRows);
}


/**
 * Compiles the ship table.
 */
var assembleShips = function () {
    $.get(web_context_ccp_data + 'ships.json', function (data) {
        $('#ship-div').empty();

        var tbl = `
        <table id="ship-table" class="evie-table ship-table">
            <thead>
                <tr class="evie-header-row">
                    <th class="evie-header" style="width: 30px;"></th>
                    <th class="evie-header w10">Name</th>
                    <th class="evie-header w10 filter">Faction</th>
                    <th class="evie-header w10 filter">Class</th>
                    <th class="evie-header w10 filter">Base Class</th>
                    <th class="evie-header w10">Capacity</th>
                    <th class="evie-header w10">Volume</th>
                </tr>
            </thead>
            <tbody class="evie-scrollbar ship-body">`;
        data.forEach(function (item, index) {
            tbl += `
                <tr class="market-row">
                    <td style="width: 30px;"><img src="${web_context_ccp_img}types/${item.id}_32.png" class="ship-icon"></td>
                    <td class="w10">${item.name}</td>
                    <td class="w10">${item.faction}</td>
                    <td class="w10">${item.class}</td>
                    <td class="w10">${item.baseClass}</td>
                    <td class="w10 right">${item.vol}</td>
                    <td class="w10 right">${item.capacity}</td>
                </tr>`;
        });

        tbl += `</tbody></table>`;
        $('#ship-div').html(tbl);
        makeTableSortable('ship-table');
        applyHeaderFilters('ship-table');
    });
}

/**
 * Displays the hauling/transport route of the product.
 * @param {} id 
 */
var displayRoute = function(sell, buy) {

    if (!mainRoute) {
        if (!sell) sell = { base: '' };
        if (!buy) buy = { base: '' };
        mainRoute = {
            sell: sell,
            buy: buy,
            income: '',
            roi: '',
            quantity: sell ? sell.quantity : buy.quantity
        }
    } else {
        if (sell) mainRoute.sell = sell;
        if (buy) mainRoute.buy = buy;
        
    }

    if (mainRoute.sell.orderId && mainRoute.buy.orderId) {
        mainRoute.quantity = mainRoute.buy.quantity < mainRoute.sell.quantity ? mainRoute.buy.quantity : mainRoute.sell.quantity;
        mainRoute.investment = quantifyPrice(calculateInvestment(mainRoute.sell, mainRoute.buy), true);
        mainRoute.income = formatQuantifiedPrice(calculateIncome(mainRoute.sell, mainRoute.buy));
        mainRoute.roi = calculateROI(mainRoute.sell, mainRoute.buy) + '%';
        mainRoute.route = findPathShortest(router, mainRoute.sell.systemId, mainRoute.buy.systemId);
    }
    mainRoute.formattedQuantity = formatQuantity(mainRoute.quantity);

    $('#market-detail-route').html(`
    <div>
        <span><b class="white">From:</b> ${mainRoute.sell.base}</span>
    </div>
    <div>
        <span><b class="white">To:</b> ${mainRoute.buy.base}</span>
    </div>
    <div>
        <span>
            <b class="white">Quantity:</b> ${mainRoute.formattedQuantity} 
            <b class="white">Investment:</b> ${mainRoute.investment}
            <b class="white">Income:</b> ${mainRoute.income}
            <b class="white">ROI:</b> ${mainRoute.roi}
        </span>
    </div>
    `);

    $('#market-detail-paths').html(paintRoute(mainRoute.route));
    makeRouteTipsMovable();
}

var paintRoute = function(route) {
    if (route == null) return '';
    let result = '';
    for (let i = 0; i < route.length; i++) {
        let system = router.find(e => e.i == route[i]);
        result += '<div class="route-spot ttip ' + (system.s.includes('-') ? 'bcnu' : 'bc' + system.s.replace('.', '')) +'">' +
        '<div class="ttip-text">' + system.n + ' <span class="' + (system.s.includes('-') ? 'cnu' : 'c' + system.s.replace('.', '')) + '">' + system.s  + '</span></div></div>';
    }
    return result;
}

/** TODO this method is not safe when displaying multiple routes, it would duplicate events on existing routes */
var makeRouteTipsMovable = function() {
    $('.route-spot.ttip').each(function() {
        let ttip = $(this);
        let ttipText = ttip.find('.ttip-text');
        ttip.on('mousemove', function (e) {
            ttipText.css('left', e.clientX + 15);
            ttipText.css('top', e.clientY);
        });
    });
}

/**
 * Displays the top detail of the product.
 * @param {} id 
 */
var displayDetail = function(id) {
    $('#market-detail-desc').empty();
    $('#market-detail-route').empty();
    $('#market-detail-paths').empty();
    mainRoute = null;
    $.get(web_context_ccp_data + 'typeIDs.json', function (data) {
        let item = data.find( item => item.id == id);
        if (item != null) {
            let gid = item.gid == null ? (web_context_ccp_img + `/types/${item.id}_64.png`) : (web_context_ccp + item.gid);
            const vol = formatQuantity(item.v);
            const dst = new Intl.NumberFormat('ru-RU', { style: 'decimal', maximumFractionDigits: 0 }).format(62500 / item.v);
            $('#market-detail-desc').html(`
            <div class="flex-row-left">
                <div>
                    <img src="${gid}" class="icon-large">
                </div>
                <div>
                    <div class="detail-label white">${item.n}</div>
                    <div class="ttip">
                        <b class="white">Volume:</b> ${vol}&nbsp;&nbsp;&nbsp;<b class="white">DST:</b> ${dst}
                        <div class="ttip-text">
                            DST is <u>rough estimation</u> of how many items will fit into DST's fleet hangar at max skill (62 000 volume).<br>
                            This is excluding cargo bay, therefore the total capacity will undoubtedly be higher.
                        </div>
                    </div>
                    <button id="market-find-best-button" class="eve-button">Find Single Best</button>
                </div>
            </div>`);
            $('.ttip').each(function() {
                let ttip = $(this);
                let ttipText = ttip.find('.ttip-text');
                ttip.on('mousemove', function (e) {
                    ttipText.css('left', e.clientX + 15);
                    ttipText.css('top', e.clientY);
                });
            });
            $('#market-find-best-button').on('click', function() {
                console.log('clicked');
                let best = bestTrade(cSellOrders, cBuyOrders);
                console.log('revenue', best);
                removeSelectionFromTable('market-table-sell');
                removeSelectionFromTable('market-table-buy');
                $('#market-sell-row-' + best.sellOrderId).addClass('selected');
                $('#market-buy-row-' + best.buyOrderId).addClass('selected');
                displayRoute(cSellOrders.find(o => { return o.orderId === best.sellOrderId}), cBuyOrders.find(o => { return o.orderId === best.buyOrderId}));
            });
        }
    });
}

/**
 * Displays buy and sell tables in the central part of the page under market tab - #market-display
 * @param {*} id of requested market article
 */
var display = function (id) {
    displayDetail(id);
    $.get('/api/market/order/' + id, function (data) {

        window.history.pushState(null, null, '/market/' + id);

        const json = JSON.parse(data);
        var buyOrders = [];
        var sellOrders = [];

        json.orders.forEach(function (item, index) {
            var sec = json.systems[item.systemId].security;
            if (sec == 1) sec = '1.0';
            sec = String(sec);
            sec = sec.substring(0, sec.startsWith('-') ? 4 : 3);
            if (sec == '-0.0') sec = '0.0';
            if (sec == '-1') sec = '-1.0';
            let base = json.stationNames[item.locationId];
            if (base == null) base = json.structureNames[item.locationId];

            var object = {
                orderId: item.orderId,
                isBuyOrder: item.isBuyOrder,
                price: item.price,
                formattedPrice: formatPrice(item.price),
                totalPrice: item.price * item.volumeTotal,
                formattedTotalPrice: quantifyPriceRightAligned(item.price * item.volumeTotal),
                range: item.range,
                systemId: item.systemId,
                quantity: item.volumeTotal,
                formattedQuantity: formatQuantity(item.volumeTotal),
                locationId: item.locationId,
                systemName: json.systems[item.systemId].name,
                base: base,
                sec: sec,
                secClass: sec.includes('-') ? 'cnu' : 'c' + sec.replace('.', '')
            }
            if (item.isBuyOrder) buyOrders.push(object);
            else sellOrders.push(object);
        });

        buyOrders.sort((a, b) => { return a.price > b.price ? -1 : 1; });
        sellOrders.sort((a, b) => { return a.price > b.price ? 1 : -1; });

        var tbl = `
        <div class="market-doubletable">
        
        <table id="market-table-sell" class="evie-table market-table">
            <thead>
                <tr class="evie-header-row">
                    <th class="evie-header w15 sort-on-next-column-as-numeric">Quantity</th>
                    <th class="hidden"></th>
                    <th class="evie-header w20 sort-on-next-column-as-numeric asc">Price</th>
                    <th class="hidden"></th>
                    <th class="evie-header w15 sort-on-next-column-as-numeric">Total</th>
                    <th class="hidden"></th>
                    <th class="evie-header w5 minch4 sort-on-next-column-as-numeric">Sec</th>
                    <th class="hidden"></th>
                    <th class="evie-header w50">Location</th>
                </tr>
            </thead>
            <tbody class="evie-scrollbar market-body">`;


        sellOrders.forEach(function (item, index) {
            tbl += `
            <tr class="market-row" id="market-sell-row-${item.orderId}">
                <td class="right w15">${item.formattedQuantity}</td>
                <td class="hidden">${item.quantity}</td>
                <td class="right w20">${item.formattedPrice}</td>
                <td class="hidden">${item.price}</td>
                <td class="right w15">${item.formattedTotalPrice}</td>
                <td class="hidden">${item.totalPrice}</td>
                <td class="right w5 minch4 ${item.secClass}">${item.sec}</td>
                <td class="hidden">${item.sec}</td>
                <td class="w50">${item.base}</td>
            </tr>`;
        });

        tbl += `</tbody>
        </table>
        <table id="market-table-buy" class="evie-table market-table">
            <thead>
                <tr class="evie-header-row">
                    <th class="evie-header w15 sort-on-next-column-as-numeric">Quantity</th>
                    <th class="hidden"></th>
                    <th class="evie-header w20 sort-on-next-column-as-numeric dsc">Price</th>
                    <th class="hidden"></th>
                    <th class="evie-header w15 sort-on-next-column-as-numeric">Total</th>
                    <th class="hidden"></th>
                    <th class="evie-header w5 min40 sort-on-next-column-as-numeric">Sec</th>
                    <th class="hidden"></th>
                    <th class="evie-header w50">Location</th>
                </tr>
            </thead>
            
            <tbody class="evie-scrollbar market-body">`;

        buyOrders.forEach(function (item, index) {
            tbl += `
            <tr class="market-row" id="market-buy-row-${item.orderId}">
                <td class="right w15">${item.formattedQuantity}</td>
                <td class="hidden">${item.quantity}</td>
                <td class="right w20">${item.formattedPrice}</td>
                <td class="hidden">${item.price}</td>
                <td class="right w15">${item.formattedTotalPrice}</td>
                <td class="hidden">${item.totalPrice}</td>
                <td class="right w5 min40 ${item.secClass}">${item.sec}</td>
                <td class="hidden">${item.sec}</td>
                <td class="w50">${item.base}</td>
            </tr>`;
        });

        tbl += `</tbody></table>`;

        $('#market-display').html(tbl);
        makeTableSortable('market-table-sell');
        makeTableSortable('market-table-buy');
        sellOrders.forEach(function(item, index) {
            $('#market-sell-row-' + item.orderId).on('click', () => {
                removeSelectionFromTable('market-table-sell');
                $('#market-sell-row-' + item.orderId).addClass('selected');
                displayRoute(item, null);
            });
        });
        buyOrders.forEach(function(item, index) {
            $('#market-buy-row-' + item.orderId).on('click', () => {
                removeSelectionFromTable('market-table-buy');
                $('#market-buy-row-' + item.orderId).addClass('selected');
                displayRoute(null, item);
            });
        });
        cSellOrders = sellOrders;
        cBuyOrders = buyOrders;
    });
}

/**
 * Creates tree-like hierarchy left menu from marketGroups.json and typeIds.json and recreates it in the #market-menu-tree.
 * Also creates the flat list of items for search purposes and places them in #market-menu-flat.
 * Only one of the two market-menu-.. is displayed at a single time depending on search. 
 */
var assembleLeftMenu = function () {
    $.get(web_context_ccp_data + 'marketGroups.json', function (data) {
        const menu = $('#market-menu');
        menu.empty();
        menu.append($('<div id="market-menu-tree"></div>'));
        menu.append($('<div id="market-menu-flat" class="hidden"></div>'));
        const flat = $('#market-menu-flat');

        var running = true;
        var remainingData = [];
        var firstRun = true;
        var lastRemainingDataCount = 0;
        while (running) {
            if (firstRun) {
                firstRun = false;
            } else {
                if (remainingData.length == 0) running = false;
                if (remainingData.length == lastRemainingDataCount) running = false;

                data = remainingData;
                lastRemainingDataCount = remainingData.length;
                remainingData = [];
            }

            if (running) for (let i = 0; i < data.length; i++) {
                var item = data[i];
                var parentElement = item.parent ? $('#market-group-container-' + item.parent) : $('#market-menu-tree');
                var groupClass = item.parent ? ' class="menu-shift"' : '';
                var menuCategory = !item.parent ? ' menu-category"' : '';
                var iconPath = "/ccp" + item.iconID;
                var icon = item.parent ? `<img src-data="${iconPath}" class="menu-icon">` : `<img src="${iconPath}" class="menu-icon"></img>`;
                if (parentElement.length) {
                    parentElement.append(
                        `<div id="market-group-${item.id}"${groupClass}>
                            <div id="market-group-label-${item.id}" class="menu-dropdown clickable menu-highlight${menuCategory}">${icon}${item.name}</div>
                            <div id="market-group-container-${item.id}" class="hidden"></div>
                        </div>`
                    );
                    $('#market-group-label-' + item.id).on('click', function () { show(this.id); });
                } else {
                    remainingData.push(item);
                }
            }
        }

        $.get(web_context_ccp_data + 'typeIDs.json', function (data) {
            for (let i = 0; i < data.length; i++) {
                var item = data[i];
                var parentElement = $('#market-group-container-' + item.g);
                if (parentElement.length) {
                    parentElement.append(
                        '<div id="market-item-' + item.id + '" class="menu-shift">' +
                            '<span id="market-item-label-' + item.id + '" class="clickable menu-highlight">' + item.n + '</span>' +
                            '<div id="market-item-container-' + item.id + '" class="hidden"></div>' +
                        '</div>'
                    );
                    $('#market-item-label-' + item.id).on('click', function () { display(this.id.replace('market-item-label-', '')); });

                    flat.append(
                        '<div id="market-flat-item-' + item.id + '" class="menu-shift">' +
                            '<span id="market-flat-item-label-' + item.id + '" class="clickable menu-highlight">' + item.n + '</span>' +
                        '</div>'
                    );

                    $('#market-flat-item-label-' + item.id).on('click', function () { display(this.id.replace('market-flat-item-label-', '')); });
                } else {
                    remainingData.push(item);
                }
            }
        });
    });
};

var onStartup = function () {
    assembleLeftMenu();
    assembleShips();

    $('#market-search').on('keyup', function () {
        var search = $(this).val().toLowerCase();
        if (search.length < 2) {
            $('#market-menu-flat').hide();
            $('#market-menu-tree').show();
        } else {
            $('#market-menu-flat').show();
            $('#market-menu-tree').hide();
            $('#market-menu-flat div').each(function () {
                var text = $(this).text().toLowerCase();
                if (text.indexOf(search) > -1) {
                    $(this).show();
                } else {
                    $(this).hide();
                }
            });
        }
    });

    $('#tab2-search-input').on('keyup', function () {
        var search = $(this).val().toLowerCase();
        $('#ship-table tbody tr').each(function () {
            var text = $(this).children().first().next().text().toLowerCase();
            if (text.indexOf(search) > -1) {
                $(this).show();
            } else {
                $(this).hide();
            }
        });
    });

    let dragging = false;
    $('#market-menu-splitter').on('mousedown', function(e) { dragging = true; });
    $('#market-menu-splitter').on('mousemove', function(e) {
        $('#market-menu-splitter-effect').css('top', e.clientY - 155);
    });
    $(document).on('mouseup', function(e) { dragging = false; $('.header-filter-list').hide(); });
    $(document).on('mousemove', function(e) { 
        if (dragging) {
            let v = e.pageX;
            $("#market-menu-splitter").css('left', (v - 3) + 'px');
            $("#market-menu").css('width', (v - 8) + 'px');
            $("#market-detail").css('left', (v + 3) + 'px');
            $("#market-display").css('left', (v + 3) + 'px');
            $("#market-search").css('width', (v - 14) + 'px');
        }
    } );
    
    $('#market-search-clear-button').click(function() {
        $('#market-search').val('');
        $('#market-menu-flat').hide();
        $('#market-menu-tree').show();
    });

    // other tabs must be hidden
    $('#nav-tab-ship').css('display', 'none');
    $('#nav-tab-about').css('display', 'none');

    $('#tab-market').click(function() {
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        $('#nav-tab-market').css('display', 'block');
        $('#nav-tab-market').siblings().css('display', 'none');
        if (!window.location.pathname.startsWith('/market')) window.history.pushState(null, null, '/market');
    });
    $('#tab-ship').click(function() {
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        $('#nav-tab-ship').css('display', 'block');
        $('#nav-tab-ship').siblings().css('display', 'none');
        if (!window.location.pathname.startsWith('/ships')) window.history.pushState(null, null, '/ships');
    });
    $('#tab-about').click(function() {
        $(this).addClass('active');
        $(this).siblings().removeClass('active');
        $('#nav-tab-about').css('display', 'block');
        $('#nav-tab-about').siblings().css('display', 'none');
        if (!window.location.pathname.startsWith('/about')) window.history.pushState(null, null, '/about');
    });

    if (window.location.pathname.startsWith('/ships')) $('#tab-ship').trigger('click');
    if (window.location.pathname.startsWith('/about')) $('#tab-about').trigger('click');
    if (window.location.pathname.startsWith('/market/')) {
        let id = window.location.pathname.split('/')[2];
        display(id);
        $('#tab-market').trigger('click');
    }

    $.get(web_context_ccp_data + 'router.json', function (data) { router = data; });

}
$(document).ready(onStartup);

/* Helper functions  */
var round = function(num) {
	return Math.round((num + Number.EPSILON) * 100) / 100;
}

var quantifySize = function(num, withSpace) {
	if (num >= 1000000000) return ((num/1000000000).toFixed(3)) + (withSpace?' ':'') + 'G';
	if (num >= 1000000) return ((num/1000000).toFixed(3)) + (withSpace?' ':'') + 'M';
	if (num >= 1000) return ((num/1000).toFixed(3)) + (withSpace?' ':'') + 'K';
	return num;
}

var quantifyPrice = function(num, withSpace) {
    if (num >= 1000000000000000) return (formatQuantifiedPrice(num/1000000000000000)) + (withSpace?' ':'') + 'Q'; // Quadrillion
    if (num >= 1000000000000) return (formatQuantifiedPrice(num/1000000000000)) + (withSpace?' ':'') + 'T'; // Trillion
	if (num >= 1000000000) return (formatQuantifiedPrice(num/1000000000)) + (withSpace?' ':'') + 'B'; // Billion
	if (num >= 1000000) return (formatQuantifiedPrice(num/1000000)) + (withSpace?' ':'') + 'M'; // Million
    return formatPrice(num);
}

var quantifyPriceRightAligned = function(num) {
    if (num >= 1000000000000000) return (formatQuantifiedPrice(num/1000000000000000)) + '&nbsp;Q'; // Quadrillion
    if (num >= 1000000000000) return (formatQuantifiedPrice(num/1000000000000)) + '&nbsp;T'; // Trillion
	if (num >= 1000000000) return (formatQuantifiedPrice(num/1000000000)) + '&nbsp;B'; // Billion
	if (num >= 1000000) return (formatQuantifiedPrice(num/1000000)) + '&nbsp;M'; // Million
    return formatPrice(num) + '&nbsp;&nbsp;';
}

var formatQuantity = function(quantity) {
    return new Intl.NumberFormat('ru-RU', { style: 'decimal' }).format(quantity);
}

var formatPrice = function(price) {
    return new Intl.NumberFormat('ru-RU', { style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(price);
}

var formatQuantifiedPrice = function(price) {
    return new Intl.NumberFormat('ru-RU', { style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 }).format(price);
}

var applyHeaderFilters = function(tableId) {
    $('#' + tableId + ' thead tr th').each(function (index, element) {
        if (!$(this).hasClass('filter')) return;
        var uniqueValues = getUniqueValuesFromColumn(tableId, index);
        if (uniqueValues.length > 0) {
            var filterId = tableId + '-filter-' + index;
            var html = `<i id="${filterId}" class="bi-filter filter-button"></i>
            <div class="header-filter-list">`;
            uniqueValues.forEach(function(value, itemIndex) {
                html += `<div id="${filterId}-item-${itemIndex}" class="header-filter-item">${value}</div>`;
            });
            html += `</div>`;
            $(this).append(html);

            $('#' + filterId).on('click', function(event) {
                console.log('click');
                $(this).next().toggle();
                event.stopPropagation();

            });
            uniqueValues.forEach(function(value, itemIndex) {
                $('#' + filterId + '-item-' + itemIndex).on('click', function(event) {
                    var search = $(this).html();
                    $('#ship-table tbody tr').each(function () {
                        var ele = $(this).children().first();
                        if (index > 0) for (var i = 0; i < index; i++) {
                            ele = ele.next();
                        }
                        var text = ele.text();
                        console.log('item click', search, text);
                        if (text.indexOf(search) > -1) {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
                    });
                    $(this).parent().hide();
                    event.stopPropagation();
                });
            });
        }
    });
}

var getUniqueValuesFromColumn = function(tableId, columnIndex) {
    const table = $('#' + tableId);
    const column = table.find('tbody tr td:nth-child(' + (columnIndex + 1) + ')');
    const uniqueValues = new Set();

    column.each(function() {
        const value = $(this).text().trim();
        uniqueValues.add(value);
    });

    const result = Array.from(uniqueValues);
    return result.sort();
}

var calculateInvestment = function(sell, buy) {
    let q = buy.quantity < sell.quantity ? buy.quantity : sell.quantity;
    return q * sell.price;
};

var calculateIncome = function(sell, buy) {
    let q = buy.quantity < sell.quantity ? buy.quantity : sell.quantity;
    return ((q * buy.price) - (q * sell.price)) - ((q * sell.price) / 100 * 2.02);
};

var calculateROI = function(sell, buy) {
    let q = buy.quantity < sell.quantity ? buy.quantity : sell.quantity;
    let i = ((q * buy.price) - (q * sell.price)) - ((q * sell.price) / 100 * 2.02);
    return formatPrice((i / (q * sell.price)) * 100);
};

function removeSelectionFromTable(tableId) {
    $('#' + tableId + ' tbody tr').each(function() {
      $(this).removeClass('selected');
    });
}

function bestTrade(sellOrders, buyOrders) {
    let bestTrade = { sellOrderId: null, buyOrderId: null, revenue: 0 };
    for (let sell of sellOrders) {
        for (let buy of buyOrders) {
            // Check if the buy price is greater than the sell price
            // TODO maube this equation should take the tax into account right here
            if (buy.price > sell.price) {
                let tradeQuantity = Math.min(sell.quantity, buy.quantity);
                let revenue = tradeQuantity * (buy.price - sell.price);
                if (revenue > bestTrade.revenue) {
                    bestTrade = {
                        sellOrderId: sell.orderId,
                        buyOrderId: buy.orderId,
                        revenue: revenue
                    };
                }
            }
        }
    }
    return { sellOrderId: bestTrade.sellOrderId, buyOrderId: bestTrade.buyOrderId };
}

var findPathShortest = function(graph, start, goal) {
    
    let queue = [start];
    let visited = new Set();
    visited.add(start);

    let predecessor = {};
    predecessor[start] = null;

    while (queue.length > 0) {
        let node = queue.shift();
        if (node === goal) {
            // Reconstruct the path from start to goal
            let path = [];
            let step = goal;
            while (step !== null) {
                path.push(step);
                step = predecessor[step];
            }
            return path.reverse(); // Return the path in correct order
        }

        let system = graph.find(e => e.i == node);
        if (system == null || system.j == null) continue;
        for (let neighbor of system.j) {
            if (!visited.has(neighbor)) {
                visited.add(neighbor);
                queue.push(neighbor);
                predecessor[neighbor] = node;
            }
        }
    }

    return []; // Return an empty array if the goal is not reachable
}