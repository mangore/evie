# EViE

Scope of this project is utility and data presentation for improved user experience of MMORPG game [EVE Online](https://www.eveonline.com/) owned by [CCP ehf](https://www.ccpgames.com/company/about/).
Provide extra features for common gamer's routines and more advanced visualization of data than the game's GUI provides.
Therefore EViE meaning EVE with some extra intel to it.

Live instance of project EViE is running at domain https://evie.works/ and is free for public use.

## License
All content of this project is released under [MIT License](/LICENSE).

Althought this project is free for use, it is dependent on proprietary content by nature. Any such content, especially that provided by CCP even in reformatted nature, should not be present in this repository.

## Deployment

Simple guideline on how all the components should be distributed in web context.

/ - root, content of web project  
/lib - here must be placed following 3rd party libraries
 - jQuery 3.7.1 https://code.jquery.com/jquery-3.7.1.min.js  

/ccp/data - preformatted JSON files with static game data  
/ccp/img/icons - [Image Export Collection (IEC)](https://developers.eveonline.com/resource) / content of Icons/items folder inside Icons.zip  
/ccp/img/types - [Image Export Collection (IEC)](https://developers.eveonline.com/resource) / content of Types folder inside Types.zip  

/api - server API endpoint, find more info on running server side in the project folder itself